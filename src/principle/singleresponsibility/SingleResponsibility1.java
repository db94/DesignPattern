package principle.singleresponsibility;

/**
 * 单一职责原则Demo1
 * 此案例违反了单一职责原则。
 */
public class SingleResponsibility1 {

    public static void main(String[] args) {

        Vehicle vehicle = new Vehicle();
        vehicle.run("摩托车");
        vehicle.run("轿车");
        vehicle.run("飞机");
        vehicle.run("潜艇");
    }
}

class Vehicle {
    public void run(String vehicle) {
        System.out.println(vehicle + "在公路上跑。");
    }
}
