package principle.singleresponsibility;

/**
 * 单一职责原则Demo3
 * 此案例违反了单一职责原则。
 * 优化Demo2。
 * 虽然在类上没有遵守单一职责原则，但是在方法上仍然遵守了单一职责原则。
 */
public class SingleResponsibility3 {

    public static void main(String[] args) {

        Vehicle2 vehicle = new Vehicle2();
        vehicle.run("摩托车");
        vehicle.run("轿车");

        vehicle.runAir("飞机");

        vehicle.runWater("潜艇");
    }
}

class Vehicle2 {
    public void run(String vehicle) {
        System.out.println(vehicle + "在公路上跑。");
    }

    public void runAir(String vehicle) {
        System.out.println(vehicle + "在天上飞。");
    }

    public void runWater(String vehicle) {
        System.out.println(vehicle + "在水中运行。");
    }
}
