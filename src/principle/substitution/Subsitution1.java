package principle.substitution;

/**
 * 里氏替换原则Demo1
 */
public class Subsitution1 {
    public static void main(String[] args) {

    }
}

class Base {

}

class A extends Base {
    public Integer func1(Integer num1, Integer num2) {
        return num1 - num2;
    }
}

class B extends Base {
    private A a = new A();

    public Integer func1(Integer num1, Integer num2) {
        return num1 + num2;
    }

    public Integer func2(Integer num1, Integer num2) {
        return func1(num1, num2) + 9;
    }

    public Integer func3(Integer num1, Integer num2) {
        return a.func1(num1, num2);
    }
}
