package principle.ocp;

/**
 * 开闭原则Demo1
 */
public class Ocp1 {
    public static void main(String[] args) {
        Circle circle = new Circle();
        circle.draw();
    }
}

interface Shape {
    void draw();
}

class Circle implements Shape{

    @Override
    public void draw() {
        System.out.println("绘制圆形。");
    }
}
