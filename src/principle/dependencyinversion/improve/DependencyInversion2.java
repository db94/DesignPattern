package principle.dependencyinversion.improve;

/**
 * 依赖倒置原则Demo2
 */
public class DependencyInversion2 {
    public static void main(String[] args) {
        Person person = new Person();
        person.receive(new Email());

        person.receive(new WeiXin());
    }
}

interface IReceiver {
    String getInfo();
}

class Email implements IReceiver {
    @Override
    public String getInfo() {
        return "电子邮件内容为：hello world";
    }
}

class WeiXin implements IReceiver{
    @Override
    public String getInfo() {
        return "微信接收到的消息为：你好";
    }
}

class Person {
    public void receive(IReceiver receiver) {
        System.out.println(receiver.getInfo());
    }
}
