package principle.dependencyinversion;

/**
 * 依赖倒置原则Demo2
 */
public class DependencyInversion1 {

    public static void main(String[] args) {
        Person person = new Person();
        person.receive(new Email());
    }
}

class Email{
    public String getInfo() {
        return "电子邮件内容为：hello world";
    }
}

class Person {
    public void receive(Email email){
        System.out.println(email.getInfo());
    }
}
