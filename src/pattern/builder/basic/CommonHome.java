package pattern.builder.basic;

public class CommonHome extends AbstractHome {
    @Override
    public void buildBasic() {
        System.out.println("打地基");
    }

    @Override
    public void buildWall() {
        System.out.println("砌墙");
    }

    @Override
    public void roofed() {
        System.out.println("封顶");
    }
}
