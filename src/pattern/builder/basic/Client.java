package pattern.builder.basic;

public class Client {
    public static void main(String[] args) {
        CommonHome commonHome = new CommonHome();
        commonHome.build();
    }
}
