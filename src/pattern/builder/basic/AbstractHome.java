package pattern.builder.basic;

public abstract class AbstractHome {
    public abstract void buildBasic();
    public abstract void buildWall();
    public abstract void roofed();

    public void build(){
        buildBasic();
        buildWall();
        roofed();
    }
}
