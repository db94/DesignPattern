package pattern.builder.improve;

public class HouseDirectior {

    public House construct(HouseBuilder hb) {
        House house;
        hb.buildBasic();
        hb.buildWalls();
        hb.roofs();
        house = hb.build();
        return house;
    }
}
