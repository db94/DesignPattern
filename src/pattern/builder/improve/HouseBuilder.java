package pattern.builder.improve;

public abstract class HouseBuilder {
    protected House house = new House();

    public abstract void buildBasic();
    public abstract void buildWalls();
    public abstract void roofs();

    public House build() {
        return  house;
    }
}
