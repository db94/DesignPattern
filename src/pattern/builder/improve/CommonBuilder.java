package pattern.builder.improve;

public class CommonBuilder extends HouseBuilder {
    @Override
    public void buildBasic() {
        house.setBasic("地基");
        System.out.println("打地基");
    }

    @Override
    public void buildWalls() {
        house.setWalls("砌墙");
        System.out.println("砌墙");
    }

    @Override
    public void roofs() {
        house.setRoof("封顶");
        System.out.println("封顶");
    }
}
