package pattern.builder.improve;

public class Client {
    public static void main(String[] args) {
        CommonBuilder commonBuilder = new CommonBuilder();
        HouseDirectior houseDirectior = new HouseDirectior();
        House house = houseDirectior.construct(commonBuilder);
        System.out.println(house);
    }
}
