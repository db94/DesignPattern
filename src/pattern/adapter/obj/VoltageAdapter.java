package pattern.adapter.obj;

public class VoltageAdapter implements IVoltage5V {

    private Voltage220V voltage220V;

    public VoltageAdapter(Voltage220V voltage220V) {
        this.voltage220V = voltage220V;
    }

    @Override
    public int output5V() {
        int dst = 0;
        dst = voltage220V.output220V()/44;
        System.out.println("输出"+dst+"伏电压");
        return dst;
    }
}
