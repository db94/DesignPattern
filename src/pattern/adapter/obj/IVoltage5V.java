package pattern.adapter.obj;

public interface IVoltage5V {
    public int output5V();
}
