package pattern.adapter.clazz;

public interface IVoltage5V {
    public int output5V();
}
