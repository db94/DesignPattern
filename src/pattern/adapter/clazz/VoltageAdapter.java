package pattern.adapter.clazz;

public class VoltageAdapter extends Voltage220V implements IVoltage5V {
    @Override
    public int output5V() {
        int dst = 0;
        dst = output220V() / 44;
        System.out.println("输出"+dst+"伏电压");
        return dst;
    }
}
