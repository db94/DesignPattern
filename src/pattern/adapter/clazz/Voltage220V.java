package pattern.adapter.clazz;

public class Voltage220V {
    public int output220V() {
        int src = 220;
        System.out.println("输出"+src+"伏电压");
        return src;
    }
}
