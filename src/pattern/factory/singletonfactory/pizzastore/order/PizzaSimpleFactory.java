package pattern.factory.singletonfactory.pizzastore.order;

import pattern.factory.singletonfactory.pizzastore.pizza.CheesePizza;
import pattern.factory.singletonfactory.pizzastore.pizza.GreekPizza;
import pattern.factory.singletonfactory.pizzastore.pizza.PepperPizza;
import pattern.factory.singletonfactory.pizzastore.pizza.Pizza;

/**
 * 简单工厂方式
 */
public class PizzaSimpleFactory {

    public static Pizza createPizza(String orderType) {
        Pizza pizza = null;
        if ("greek".equals(orderType)) {
            pizza = new GreekPizza();
            pizza.setName("希腊披萨");
        } else if ("cheese".equals(orderType)) {
            pizza = new CheesePizza();
            pizza.setName("奶酪披萨");
        } else if ("pepper".equals(orderType)){
            pizza = new PepperPizza();
            pizza.setName("胡椒披萨");

        }
        return pizza;

    }
}
