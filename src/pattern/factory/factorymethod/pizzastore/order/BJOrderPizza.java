package pattern.factory.factorymethod.pizzastore.order;

import pattern.factory.factorymethod.pizzastore.pizza.BJCheesePizza;
import pattern.factory.factorymethod.pizzastore.pizza.Pizza;

public class BJOrderPizza extends OrderPizza {
    @Override
    public Pizza createPizza(String orderType) {
        Pizza pizza = null;
        if ("cheese".equals(orderType)){
            pizza = new BJCheesePizza();
        }
        return pizza;
    }
}
