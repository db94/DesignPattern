package pattern.factory.factorymethod.pizzastore.order;

import pattern.factory.factorymethod.pizzastore.pizza.LandonPepperPizza;
import pattern.factory.factorymethod.pizzastore.pizza.Pizza;

public class LandonOrderPizza extends OrderPizza {
    @Override
    public Pizza createPizza(String orderType) {
        Pizza pizza = null;
        if ("pepper".equals(orderType)){
            pizza = new LandonPepperPizza();
        }
        return pizza;
    }
}
