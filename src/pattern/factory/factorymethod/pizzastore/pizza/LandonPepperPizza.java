package pattern.factory.factorymethod.pizzastore.pizza;

public class LandonPepperPizza extends Pizza {
    @Override
    public void prepare() {
        setName("伦敦的胡椒披萨");
        System.out.println("准备伦敦的胡椒披萨");
    }
}
