package pattern.factory.abstractfactory.pizzastore.order;

import pattern.factory.abstractfactory.pizzastore.pizza.BJCheesePizza;
import pattern.factory.abstractfactory.pizzastore.pizza.Pizza;

public class BJFactory implements AbsFactory {
    @Override
    public Pizza createPizza(String orderType) {
        Pizza pizza= null;
        if ("cheese".equals(orderType)){
            pizza = new BJCheesePizza();
        }
        return pizza;
    }
}
