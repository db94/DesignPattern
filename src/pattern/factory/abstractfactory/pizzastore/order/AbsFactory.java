package pattern.factory.abstractfactory.pizzastore.order;

import pattern.factory.abstractfactory.pizzastore.pizza.Pizza;

public interface AbsFactory {

    Pizza createPizza(String orderType);
}
