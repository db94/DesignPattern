package pattern.factory.abstractfactory.pizzastore.order;

import pattern.factory.abstractfactory.pizzastore.pizza.LandonPepperPizza;
import pattern.factory.abstractfactory.pizzastore.pizza.Pizza;

public class LDFactory implements AbsFactory {
    @Override
    public Pizza createPizza(String orderType) {
        Pizza pizza = null;
        if ("pepper".equals(orderType)){
            pizza = new LandonPepperPizza();
        }
        return pizza;
    }
}
