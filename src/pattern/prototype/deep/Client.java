package pattern.prototype.deep;

public class Client {
    public static void main(String[] args) throws CloneNotSupportedException {
        DeepPrototype deepPrototype = new DeepPrototype();
        DeepCloneableTarget deepCloneableTarget = new DeepCloneableTarget("深拷贝", "pattern.prototype.deep.DeepCloneableTarget");
        deepPrototype.setName("拷贝");
        deepPrototype.setDeepCloneableTarget(deepCloneableTarget);
        System.out.println(deepPrototype);

        DeepPrototype clone = deepPrototype.clone();
        System.out.println(clone);
    }
}
