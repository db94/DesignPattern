package pattern.prototype.improve;

public class PrototypeTestDemo {
    public static void main(String[] args) {
        Sheep sheep = new Sheep("tom", 1, "白色");
        sheep.setFriend(new Sheep("lili",2,"黑色"));
        Sheep tom = sheep.clone();
        System.out.println(tom);
        System.out.println(sheep);
        sheep.setAge(12);
        System.out.println(tom);
        System.out.println(sheep);
    }
}
