package pattern.singleton.type1;

/**
 * 饿汉式（静态代码块）
 */
public class SingletonDemo2 {
}

class Singleton1{
    private static Singleton1 instance;
    static {
        instance = new Singleton1();
    }

    // 1。私有化构造方法
    private Singleton1() {

    }

    public Singleton1 getInstance() {
        return instance;
    }
}
