package pattern.singleton.type1;

/**
 * 单例模式8中写法，第一种
 * 饿汉式（静态变量）
 */
public class SingletonDemo1 {
    public static void main(String[] args) {

    }
}

class Singleton {

    // 2.创建类的实例
    private final static Singleton singleton = new Singleton();

    // 1.私有化构造方法
    private Singleton() {

    }

    // 3.提供一个静态方法，返回类的实例
    public Singleton getInstance() {
        return singleton;
    }
}
