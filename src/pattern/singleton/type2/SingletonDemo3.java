package pattern.singleton.type2;

/**
 * 懒汉式（线程不安全）
 */
public class SingletonDemo3 {
}

class Singleton3 {
    private static Singleton3 instance;

    // 1.私有化构造方法
    private Singleton3() {

    }

    public static Singleton3 getInstance() {
        if (instance == null) {
            instance = new Singleton3();
        }
        return instance;
    }
}
