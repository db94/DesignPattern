package pattern.singleton.type7;

/**
 * 静态内部类完成(推荐使用)
 */
public class SingletonDemo7 {
}

class Singleton7 {
    private Singleton7() {}

    private static class SingletonInstance{
        private static final Singleton7 INSTANCE = new Singleton7();
    }

    public static Singleton7 getInstance(){
        return SingletonInstance.INSTANCE;
    }
}
