package pattern.singleton.type3;

/**
 * 双重检查（推荐使用）
 */
public class SingletonDemo6 {
}

class Singleton6 {
    private static volatile Singleton6 instance;

    private Singleton6() {}

    public static Singleton6 getInstance(){
        if (instance == null) {
            synchronized (Singleton6.class){
                if (instance == null){
                    instance = new Singleton6();
                }
            }
        }
        return instance;
    }
}
